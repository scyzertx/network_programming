#include <sys/socket.h>
#include <ctype.h>
#include <unistd.h>
#include <stdio.h>
int main(){

		
	int socketvector[2]; //pair of socket descriptors
	socketpair(AF_LOCAL,SOCK_STREAM,0,socketvector); //creates an unname pair of connected sockets int the specified domain
	char readbuffer;

	if( fork() == 0){
		//child
		//read() attempts to read up to count bytes from file descriptor fd into buffer starting at buf
		read(socketvector[0],&readbuffer,1);
		printf("Child: read %c \n",readbuffer );
		readbuffer = toupper(readbuffer);
		//the write utility allows you to communicate with other users, by copying line from your terminal to theirs
		write(socketvector[0],&readbuffer,1);
		printf("Child: sent %c \n",readbuffer);
	}else{
		//parent
		write(socketvector[1],"b",1);
		printf("Parent: sent 'b' \n");
		read(socketvector[1],&readbuffer,1);
		printf("Parent: read %c \n", readbuffer);
		//wait() this function waits for the first child to die. The return value is that of the wait(2) system call
		wait(NULL); /* wait for child to exit() */
	}
	

	return 0;
}